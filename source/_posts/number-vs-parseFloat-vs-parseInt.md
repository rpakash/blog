---
title: Number vs parseFloat vs parseInt
date: 2022-10-16 12:09:49
tags:
---

## Number

The Number() method converts the input string to a number. It takes only one parameter. Numeric conversion using a plus + or Number() is strict. If a value is not exactly a number, it fails and gives `NaN`. This method can also convert hexadecimal numbers and octal numbers to decimal numbers, It only accepts these two `0x` and `0o` non-numerical characters that represent hexadecimal and octal numbers. Number returns `NaN` when the string contains invalid characters. The Number() can be called as a constructor which returns an Object. The Short Method for using the Number() method is to use `+/-` with the string.

{% codeblock lang:javascript %}
Number("5") // 5
Number("0xF") //15 Hexadecimal to decimal
Number("0o15") //13 Octal number to decimal
Number("12px") // NaN

+"0xF" // 15 Hexadecimal to decimal

typeof new Number("15") // Object
typeof Number("15") //number
{% endcodeblock %}

## parseInt

parseInt() method parses the input string and returns the integer of the given base. It takes two parameters, one is the string to be converted, another one is the base. This method parses the string until any character other than the number occurs, and returns the truncated number. If the string starts with `0x` then it is parsed as a hexadecimal number, otherwise, it is parsed as a decimal number. This method does not parse the numbers after the decimal point. Returns `NaN` if it cannot parse the string

{% codeblock lang:javascript %}
parseInt("5") // 5
parseInt("125a56") // 125 parsed till non numerical element occurred
parseInt("0xF") //15 parses as a hexadecimal number
parseInt("F",16) //15 parses as a hexadecimal number
parseInt("F") //NaN
parseInt("0o15") //0
parseInt("12px") //12
parseInt("123.33b") // 123
parseInt("b23") // NaN
{% endcodeblock %}

## parseFloat

parseFloat() method parses the input string to a floating point number. It takes only one parameter. This method parses the string until a non-numerical element occurs other than `.`. This method does not converts hexadecimal or octal number. It returns `NaN` if it fails to convert the string to the number

{% codeblock lang:javascript %}
parseFloat("5") // 5
parseFloat("125a56") // 125 parsed till non numerical element occurred
parseFloat("0xF") //0
parseFloat("F") //NaN
parseFloat("0o15") //0
parseFloat("12px") //12
parseFloat("123.33b") // 123.33
{% endcodeblock %}

## Number vs parseFloat vs parseInt

- ### When the string is empty

  When the input string is empty, the Number() method returns `0`, whereas parseInt and parseFloat returns `NaN`
  {% codeblock lang:javascript %}
  Number("") // 0

  parseInt("") // NaN

  parseFloat("") // NaN
  {% endcodeblock %}

- ### When the input string contains only white spaces

  When the input string contains only white spaces the Number() method returns `0`, whereas parseInt and parseFloat return `NaN`.

  {% codeblock lang:javascript %}
  Number(" ") // 0

  parseInt(" ") // NaN

  parseFloat(" ") // NaN
  {% endcodeblock %}

- ### When the input string contains spaces between numbers

  When the input string contains spaces between numbers, the Number() method returns `NaN`, whereas the parseInt and parseFloat return numbers that occurred before the white space.

  {% codeblock lang:javascript %}
  Number(" 1 1 ") // NaN

  parseInt(" 1 1 ") // 1

  parseFloat(" 1 1 ") // 1
  {% endcodeblock %}

- ### Other differences to consider

  Number returns `0` for null, false and empty Array, whereas parseInt and parseFloat returns `NaN`
  {% codeblock lang:javascript %}
  Number(false) // 0
  Number(null) // 0
  Number([]) // 0

  parseInt(false) // NaN
  parseInt(null) // NaN
  parseInt([]) // NaN

  parseFloat(false) // NaN
  parseFloat(null) // NaN
  parseFloat([]) // NaN
  {% endcodeblock %}
