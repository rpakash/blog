---
title: Named function vs Anonymous function
date: 2022-10-16 21:55:26
tags:
---

## 1. Function structure

- ### Named function

  The named function will have an identifier attached to it.

  {% codeblock lang:javascript %}
  function sayHi() {
  // code inside function
  }
  {% endcodeblock %}

  In the above function, the `sayHi` is the identifier. The name can be anything but should be unique.

- ### Anonymous function

  Anonymous functions don't have an identifier.

  {% codeblock lang:javascript %}
  function () {
  // code inside function
  }
  {% endcodeblock %}

  Anonymous functions should be assigned to the variable or invoked immediately, or JavaScript will give an error.

  {% codeblock lang:javascript%}
  // Function expression
  const sayHi = function () {
  // code inside function
  }

  //Immediately invoked function expression
  (function () {
  //code inside function
  })()
  {% endcodeblock %}

  If the function is assigned to a variable then it is called a Function expression. If the function is invoked immediately by putting the function inside parenthesis and invoking it is called IIFE

## 2. Function hoisting

- ### Named function

  Named functions are hoisted, they can be accessed before declaring.
  {% codeblock lang:javascript %}
  sayHi() // Hi

  function sayHi() {
  return "Hi";
  }
  {% endcodeblock %}

- ### Anonymous function

  Anonymous functions are not hoisted, they cannot be accessed before declaring.

  {% codeblock lang:javascript %}
  sayHi() // ReferenceError: Cannot access 'sayHi' before initialization

  let sayHi = function () {
  return "Hi";
  }
  {% endcodeblock %}

## 3. Function.name property

- ### Named function

  Named functions will have the same name as Identifier, even if it is assigned to a variable
  {% codeblock lang:javascript %}
  function sayHi() {
  return "Hi";
  };

  sayHi.name // sayHi

  let tellHi = function sayHi() {
  return "Hi";
  };

  console.log(tellHi.name); // sayHi
  {% endcodeblock %}

- ### Anonymous function

  As the anonymous function doesn't have an Identifier, the function.name will be the variable name to which the function is assigned
  or it will be an empty string.

  {% codeblock lang:javascript %}
  let sayHi = function {
  return "Hi";
  };

  console.log(sayHi.name); // sayHi

  console.log(function () {}.name) // ""

  {% endcodeblock %}
